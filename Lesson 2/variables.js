const firstName = "Nick";

const lastName = "Myers";

const fullName = firstName + " " + lastName;

console.log(fullName);

// in general don't use var unless the project is already using it
// var name = firstName + " " + lastName;

// modern variables that change over time

let name = "Nick";

name += " Myers";

console.log(name);
// this is bad TypeScript solves this
name = 0;

console.log(name);
// this is bad, TypeScript solves this
name = true;

console.log(name);
